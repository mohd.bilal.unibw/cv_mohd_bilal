/**
 * @file CVMain.c
 *
 * @author FW Profile code generator version 5.29
 * @date Created on: Apr 15 2023 10:2:0
 */

/** CV function definitions */
#include "CV.h"

/** FW Profile function definitions */
#include "FwPrConstants.h"
#include "FwPrDCreate.h"
#include "FwPrConfig.h"
#include "FwPrCore.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* ----------------------------------------------------------------------------------------------------------------- */

/* Action for node Y2007. */
void MbCrTopSchool(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y2007.\n");
}

/* Action for node Y2009. */
void MbCrEnterCollege(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y2009.\n");
}

/* Action for node Y2011. */
void MbCrWorldSolarChallenge(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y2011.\n");
}

/* Action for node Y2013. */
void MbCrTataMotors(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y2013.\n");
}

/* Action for node Y2015. */
void MbCrStartEV(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y2015.\n");
}

/* Action for node Y2016. */
void MbCrErasmus(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y2016.\n");
}

/* Action for node Y2016N17. */
void MbCrSpaceMaster(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y2016N17.\n");
}

/* Action for node Y2017. */
void MbCrBexus(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y2017.\n");
}

/* Action for node Y2018. */
void MbCrMasterThesis(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y2018.\n");
}

/* Action for node Y2019toY2021. */
void MbCrTOM(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y2019toY2021.\n");
}

/* Action for node Y2020toY2021. */
void MbCrFormationFlying(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y2020toY2021.\n");
}

/* Action for node Y2021. */
void MbCrSeranis(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y2021.\n");
}

/* Action for node Y2022toNow. */
void MbCrObsw(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y2022toNow.\n");
}

/* Action for node Y2023. */
void MbCrSw(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y2023.\n");
}

/* Action for node Y2022. */
void MbCrPyObsw(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y2022.\n");
}

/* Action for node Y2023toEOL. */
void MbCrCoolStuff(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y2023toEOL.\n");
}

/* Action for node Y1993. */
void MbBOL(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Action for node Y1993.\n");
}

/* Guard on the Control Flow from Y2022toNow to DECISION1. */
FwPrBool_t IsAccepted(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Guard on the Control Flow from Y2022toNow to DECISION1.\n");
	return rand()>RAND_MAX/2 ? 1 : 0;
}

/* Guard on the Control Flow from DECISION1 to Y2023. */
FwPrBool_t MbCrAccepted(FwPrDesc_t prDesc)
{	(void)prDesc;
	printf("  Guard on the Control Flow from DECISION1 to Y2023.\n");
	return rand()>RAND_MAX/2 ? 1 : 0;
}

/* ----------------------------------------------------------------------------------------------------------------- */

int main(void)
{
	/** Define the procedure descriptor (PRD) */
	FwPrDesc_t prDesc = CVCreate(NULL);

	srand(time(NULL));

	/** Check that the procedure is properly configured */
	if (FwPrCheck(prDesc) != prSuccess) {
		printf("The procedure CV is NOT properly configured ... FAILURE\n");
		return EXIT_FAILURE;
	}

	printf("The procedure CV is properly configured ... SUCCESS\n");

	/** Start the procedure, and execute it a few times */
	FwPrStart(prDesc);
	FwPrExecute(prDesc);
	FwPrExecute(prDesc);
	FwPrExecute(prDesc);
	FwPrExecute(prDesc);

	return EXIT_SUCCESS;
}