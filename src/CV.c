/**
 * @file CV.c
 *
 * @author FW Profile code generator version 5.29
 * @date Created on: Apr 15 2023 10:2:0
 */

#include "CV.h"

/* FW Profile function definitions */
#include "FwPrDCreate.h"
#include "FwPrConfig.h"
#include "FwPrCore.h"

/* CV function definitions */
#include <stdlib.h>

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CVCreate(void* prData)
{
	const FwPrCounterU2_t DECISION1 = 1;		/** The identifier of decision node DECISION1 in procedure CV */
	const FwPrCounterU2_t N_OUT_OF_DECISION1 = 2;	/* The number of control flows out of decision node DECISION1 in procedure CV */

	/** Create the procedure */
	FwPrDesc_t prDesc = FwPrCreate(
		17,	/* N_ANODES - The number of action nodes */
		1,	/* N_DNODES - The number of decision nodes */
		20,	/* N_FLOWS - The number of control flows */
		17,	/* N_ACTIONS - The number of actions */
		3	/* N_GUARDS - The number of guards */
	);

	/** Configure the procedure */
	FwPrSetData(prDesc, prData);
	FwPrAddActionNode(prDesc, CV_Y2007, &MbCrTopSchool);
	FwPrAddActionNode(prDesc, CV_Y2009, &MbCrEnterCollege);
	FwPrAddActionNode(prDesc, CV_Y2011, &MbCrWorldSolarChallenge);
	FwPrAddActionNode(prDesc, CV_Y2013, &MbCrTataMotors);
	FwPrAddActionNode(prDesc, CV_Y2015, &MbCrStartEV);
	FwPrAddActionNode(prDesc, CV_Y2016, &MbCrErasmus);
	FwPrAddActionNode(prDesc, CV_Y2016N17, &MbCrSpaceMaster);
	FwPrAddActionNode(prDesc, CV_Y2017, &MbCrBexus);
	FwPrAddActionNode(prDesc, CV_Y2018, &MbCrMasterThesis);
	FwPrAddActionNode(prDesc, CV_Y2019toY2021, &MbCrTOM);
	FwPrAddActionNode(prDesc, CV_Y2020toY2021, &MbCrFormationFlying);
	FwPrAddActionNode(prDesc, CV_Y2021, &MbCrSeranis);
	FwPrAddActionNode(prDesc, CV_Y2022toNow, &MbCrObsw);
	FwPrAddActionNode(prDesc, CV_Y2023, &MbCrSw);
	FwPrAddDecisionNode(prDesc, DECISION1, N_OUT_OF_DECISION1);
	FwPrAddActionNode(prDesc, CV_Y2022, &MbCrPyObsw);
	FwPrAddActionNode(prDesc, CV_Y2023toEOL, &MbCrCoolStuff);
	FwPrAddActionNode(prDesc, CV_Y1993, &MbBOL);
	FwPrAddFlowActToAct(prDesc, CV_Y2007, CV_Y2009, NULL);
	FwPrAddFlowActToAct(prDesc, CV_Y2009, CV_Y2011, NULL);
	FwPrAddFlowActToAct(prDesc, CV_Y2011, CV_Y2013, NULL);
	FwPrAddFlowActToAct(prDesc, CV_Y2013, CV_Y2015, NULL);
	FwPrAddFlowActToAct(prDesc, CV_Y2015, CV_Y2016, NULL);
	FwPrAddFlowActToAct(prDesc, CV_Y2016, CV_Y2016N17, NULL);
	FwPrAddFlowActToAct(prDesc, CV_Y2016N17, CV_Y2017, NULL);
	FwPrAddFlowActToAct(prDesc, CV_Y2017, CV_Y2018, NULL);
	FwPrAddFlowActToAct(prDesc, CV_Y2018, CV_Y2019toY2021, NULL);
	FwPrAddFlowActToAct(prDesc, CV_Y2019toY2021, CV_Y2020toY2021, NULL);
	FwPrAddFlowActToAct(prDesc, CV_Y2020toY2021, CV_Y2021, NULL);
	FwPrAddFlowActToAct(prDesc, CV_Y2021, CV_Y2022, NULL);
	FwPrAddFlowActToDec(prDesc, CV_Y2022toNow, DECISION1, &IsAccepted);
	FwPrAddFlowActToAct(prDesc, CV_Y2023, CV_Y2023toEOL, NULL);
	FwPrAddFlowDecToAct(prDesc, DECISION1, CV_Y2023, &MbCrAccepted);
	FwPrAddFlowDecToAct(prDesc, DECISION1, CV_Y2022toNow, &MbCrReflexRejects);
	FwPrAddFlowActToAct(prDesc, CV_Y2022, CV_Y2022toNow, NULL);
	FwPrAddFlowActToFin(prDesc, CV_Y2023toEOL, NULL);
	FwPrAddFlowActToAct(prDesc, CV_Y1993, CV_Y2007, NULL);
	FwPrAddFlowIniToAct(prDesc, CV_Y1993, NULL);

	return prDesc;
}

